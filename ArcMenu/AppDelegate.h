//
//  AppDelegate.h
//  ArcMenu
//

#import <UIKit/UIKit.h>
#import "ArcMenu.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate, ArcMenuDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
