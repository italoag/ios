//
//  ArcMenu.h
//  ArcMenu

#import <UIKit/UIKit.h>
#import "ArcMenuItem.h"

@protocol ArcMenuDelegate;


@interface ArcMenu : UIView <ArcMenuItemDelegate>
{
    NSArray *_menusArray;
    int _flag;
    NSTimer *_timer;
    ArcMenuItem *_addButton;
    
    id<ArcMenuDelegate> _delegate;
    BOOL _isAnimating;
}
@property (nonatomic, copy) NSArray *menusArray;
@property (nonatomic, getter = isExpanding) BOOL expanding;
@property (nonatomic, assign) id<ArcMenuDelegate> delegate;

@property (nonatomic, retain) UIImage *image;
@property (nonatomic, retain) UIImage *highlightedImage;
@property (nonatomic, retain) UIImage *contentImage;
@property (nonatomic, retain) UIImage *highlightedContentImage;

@property (nonatomic, assign) CGFloat nearRadius;
@property (nonatomic, assign) CGFloat endRadius;
@property (nonatomic, assign) CGFloat farRadius;
@property (nonatomic, assign) CGPoint startPoint;
@property (nonatomic, assign) CGFloat timeOffset;
@property (nonatomic, assign) CGFloat rotateAngle;
@property (nonatomic, assign) CGFloat menuWholeAngle;
@property (nonatomic, assign) CGFloat expandRotation;
@property (nonatomic, assign) CGFloat closeRotation;

- (id)initWithFrame:(CGRect)frame menus:(NSArray *)aMenusArray;
@end

@protocol ArcMenuDelegate <NSObject>
- (void)ArcMenu:(ArcMenu *)menu didSelectIndex:(NSInteger)idx;
@optional
- (void)ArcMenuDidFinishAnimationClose:(ArcMenu *)menu;
- (void)ArcMenuDidFinishAnimationOpen:(ArcMenu *)menu;
@end