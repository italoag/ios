//
//  ArcMenuItem.h
//  ArcMenu
//
#import <UIKit/UIKit.h>

@protocol ArcMenuItemDelegate;

@interface ArcMenuItem : UIImageView
{
    UIImageView *_contentImageView;
    CGPoint _startPoint;
    CGPoint _endPoint;
    CGPoint _nearPoint; // perto
    CGPoint _farPoint; // longe
    
    id<ArcMenuItemDelegate> _delegate;
}

@property (nonatomic, retain, readonly) UIImageView *contentImageView;

@property (nonatomic) CGPoint startPoint;
@property (nonatomic) CGPoint endPoint;
@property (nonatomic) CGPoint nearPoint;
@property (nonatomic) CGPoint farPoint;

@property (nonatomic, assign) id<ArcMenuItemDelegate> delegate;

- (id)initWithImage:(UIImage *)img 
   highlightedImage:(UIImage *)himg
       ContentImage:(UIImage *)cimg
highlightedContentImage:(UIImage *)hcimg;


@end

@protocol ArcMenuItemDelegate <NSObject>
- (void)ArcMenuItemTouchesBegan:(ArcMenuItem *)item;
- (void)ArcMenuItemTouchesEnd:(ArcMenuItem *)item;
@end